function createSelect() {
	serverGet("select.php", "depart");
}

function selectDepart() {
	serverGet("select.php", "puesto");
}

function puesto() {
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			document.getElementById("puesto").innerHTML = request.responseText;
		}
	}
	request.open("POST","http://localhost/Practica/PHP/crear.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send("puesto=" + document.getElementById('depart').value);
}


function insert() {
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			document.getElementById("consult").innerHTML=request.responseText;
		}
	}
	request.open("POST","http://localhost/Practica/PHP/insert.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send("nombre=" + document.getElementById('nombre').value +
		"&mail=" + document.getElementById('mail').value + 
		"&telefono=" + document.getElementById('telefono').value + 
		"&puesto=" + document.getElementById('puesto').value + 
		"&depart=" + document.getElementById('depart').value);
}

function buscarDepart() {
	var request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			document.getElementById("consult").innerHTML = request.responseText;
			var texto = request.responseText, json = JSON.parse(texto), limite = json.empleados.length; respuesta = "";
			for (i=0;i<limite; i++) {
				respuesta += "<tr> <td>" + json.empleados[i].puesto + "</td> <td>" + json.empleados[i].Nombre + 
				"</td> <td>" + json.empleados[i].Mail + " </td>" + "<td>" + json.empleados[i].Telefono + "</tr>";
			}
			document.getElementById("consult").innerHTML = "<br> <table class='table'> <tr>  <th> Pusto </th> <th> " + 
			"Nombre </th> <th> Mail </th> <th> Telefono </th> </tr>" + respuesta + "</table>";
		}
	}
	request.open("POST", "http://localhost/Practica/PHP/buscar.php", true);
	request.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	request.send("depart=" + document.getElementById('puesto').value);
}

function serverGet(file, id) {
	let request;
	request = new XMLHttpRequest();
	request.onreadystatechange = function() {
		if (request.readyState == 4 && request.status == 200) {
			document.getElementById(id).innerHTML = request.responseText;
		}
	}
	request.open("get", "http://localhost/Practica/PHP/" + file, true);
	request.send();
}

function consultar() {
	var html = '<div><select id="puesto" onchange="buscarDepart()"></select></div><div id="consult"></div><br>';
	document.getElementById("null").innerHTML = html;
	document.getElementById("buttonCrear").style.display = "none";
	document.getElementById("buttonConsultar").style.display = "none";
	selectDepart();
}

function crear() {
	var html = '<div id="menu"><select id="depart" onchange="puesto()"></select><br><br><select id="puesto"></select><br><h2>Nombre</h2><br><input type="text" id="nombre"><br><h2>Mail</h2><br><input type="text" id="mail"><br><h2>telefono</h2><br><input type="text" id="telefono"><br><br><button type="button" id="enviar" class="btn-primary" onclick="insert()">enviar</button><div id="consult"></div><br>';
	document.getElementById("null").innerHTML = html;
	document.getElementById("buttonCrear").style.display = "none";
	document.getElementById("buttonConsultar").style.display = "none";
	createSelect();
}