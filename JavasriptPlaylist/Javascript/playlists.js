let playlists = [
    {
        ID: "1",
        Name: "Crazy World",
        Author: "Scorpions",
        Genre: "Hard Rock",
        Songs: "1,4,7,12",
    },
    {
        ID: "2",
        Name: "Hybrid Theory",
        Author: "Linkin Park",
        Genre: "Alternative",
        Songs: "5,8,9,15,18,20"
    },
    {
        ID: "3",
        Name: "Back in Black",
        Author: "AC/DC",
        Genre: "Rock",
        Songs: "2,6,14,19"
    },
    {
        ID: "4",
        Name: "Nightcore",
        Author: "Various",
        Genre: "Remix",
        Songs: "3,10,11,21,24"
    },
    {
        ID: "5",
        Name: "Toxicity",
        Author: "System of a Dawn",
        Genre: "Heavy Metal",
        Songs: "13,16,22"
    }
];