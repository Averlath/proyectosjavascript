angular.module('playListsApp', []).controller('controller', ['$scope', function ($scope) {
    var helper = this, psongs = [];

    this.psongs = psongs;
    this.playlists = playlists;
    this.songs = songs;

    this.removeList = function(item) {
        angular.forEach(this.playlists, function(list) {
            let array = document.getElementsByClassName(item);
            for (i=0; i<array.length; i++) {
                array[i].style.display = "none";
            }
        });
    }

    this.removePSong = function(item) {
        angular.forEach(this.psongs, function(list) {
            let array = document.getElementsByClassName(item);
            for (i=0; i<array.length; i++) {
                array[i].style.display = "none";
            }
        });
    }

    this.removeSong = function(item) {
        angular.forEach(this.songs, function(list) {
            let array = document.getElementsByClassName(item);
            for (i=0; i<array.length; i++) {
                array[i].style.display = "none";
            }
        });
    }

    this.selectPlaylist = function(songs) {
        if (psongs.length !== 0) {
            location.reload();
            /*
            * De cualquier manera que intente poner el array psongs a [], no me funciono.
            * Mi idea era, cuando se haga click sobre un playlist, mostrarlo.
            * En caso de hacer click sobre otro, reset-ear el array antes de ejecutar el forEach.
            * Pero por alguna razon, no se volvia a llenar despues del reset.
            */
        }

        let songslist = songs.split(",");
        angular.forEach(this.songs, function(list) {
            for (i=0; i<songslist.length; i++) {
                if (list.ID === songslist[i]) {
                    psongs.push({
                        ID: list.ID,
                        Name: list.Name,
                        Author: list.Author,
                        Genre: list.Genre,
                        Duration: list.Duration
                    });
                }
            }
        });
    }

    this.addSong = function() {
        this.psongs.push({
            Name: this.name,
            Author: this.author,
            Genre: this.genre,
            Duration: this.duration
        });
    };

    this.orderColumn = function(orderBy) {
        return this.myOrder = orderBy;
    }
}]);